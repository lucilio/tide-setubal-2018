/**
 * @file Webpack configuration file
 * @author Mike Joyce [hello@mikejoyce.io]
 */

/**
 * Webpack
 * @type {Object}
 * @see {@link https://webpack.js.org/}
 */
const webpack = require('webpack');

/**
 * Path
 * @type {Object}
 * @see {@link https://nodejs.org/api/path.html#path_path}
 */
const path = require('path');

/**
 * Plugins
 * @type {Object}
 */
const plugins = {

  /**
   * Extract Text
   * @type {[Object}
   * @see {@link https://www.npmjs.com/package/extract-text-webpack-plugin}
   */
  extractText: require('extract-text-webpack-plugin'),

  /**
   * Clean Webpack
   * @type {Object}
   * @see {@link @see {@link https://www.npmjs.com/package/clean-webpack-plugin}}
   */
  cleanWebpack: require('clean-webpack-plugin'),

  /**
   * Uglify JS
   * @type {Object}
   * @see {@link https://www.npmjs.com/package/uglifyjs-webpack-plugin}
   */
  uglifyJs: require('uglifyjs-webpack-plugin'),

  /**
   * Optimize CSS
   * @type {Object}
   * @see {@link https://www.npmjs.com/package/optimize-css-assets-webpack-plugin}
   */
  optimizeCSS: require('optimize-css-assets-webpack-plugin'),

  /**
   * Imagemin
   * @type {Object}
   * @see {@link https://www.npmjs.com/package/imagemin-webpack-plugin}
   */
  imageMin: require('imagemin-webpack-plugin').default,

  /**
   * Autoprefixer
   * @type {Object}
   * @see {@link https://www.npmjs.com/package/autoprefixer}
   */
  autoprefixer: require('autoprefixer'),

  /**
   * HTML Webpack Plugin
   * @type {Object}
   * @see {@link https://www.npmjs.com/package/autoprefixer}
   */
  HtmlWebpackPlugin: require('html-webpack-plugin')

};

/**
 * Config
 * @type {Object}
 */
const config = {
  src: path.join(__dirname, '/src'),
  dist: path.join(__dirname, '/dist')
};

module.exports = {
  output: {
    path: config.dist,
    filename: 'js/app.js',
  },
  resolve: {
    extensions: ['.js'],
    modules: [path.resolve(__dirname, 'src/js/modules'), 'node_modules']
  },
  module: {
    rules: [
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              useRelativePath: true,
              publicPath: '../fonts',
              outputPath: 'fonts'
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          useRelativePath: true,
          publicPath: '../img',
          outputPath: 'img'
        }
      },
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [ '@babel/preset-env', '@babel/react' ]
            }
          }
        ]
      },
      {
        test: /\.s?css$/,
        use: plugins.extractText.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [
                  plugins.autoprefixer
                ]
              }
            },
            {
              loader: 'resolve-url-loader',
              options: {
                debug: false
              }
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: [
                  'node_modules/',
                  'node_modules/motion-ui/src',
                  'node_modules/foundation-sites/scss',
                  'src/scss'
                ],
                sourceMap: true
              }
            }
          ]
        },)
      }
    ]
  },
  optimization: {
    minimizer: [
      new plugins.uglifyJs({
        cache: true,
        parallel: true,
        sourceMap: true
      }),
      new plugins.optimizeCSS({}),
    ]
  },
  plugins: [
    new plugins.cleanWebpack([
      `${config.dist}/*`
    ]),
    new plugins.extractText(
      'css/app.css'
    ),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new plugins.imageMin({
      test: 'media/**'
    }),
    new plugins.HtmlWebpackPlugin({
      title: 'Relat&oacute;rio 2018 :: Funda&ccedil;&atilde;o Tide Set&uacute;bal'
    }),
  ]
};
