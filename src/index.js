import jQuery from 'jquery';
import Foundation from 'foundation-sites';
import React from 'react';
import ReactDOM from 'react-dom';
import { contents, navigation, networkData } from './data.json';
import { Modal } from './js/modal.js';
import { Menu, MenuItem, NavBar } from './js/navigation.js';
import { Image, importedImages } from './js/images.js';
import { Bubbles } from './js/bubbles.js';
import { NetworkGraph } from './js/network.js';
import './styles/webfonts/Roboto-Regular.ttf';
import './styles/webfonts/OpenSans-Bold.ttf';
import './styles/index.scss';

/**
 * Get Foundation ready (including binding jQuery)
 *
 **/

window.jQuery = jQuery;
jQuery(document).ready( function($){
	$(document).foundation();
} )

/**
 * Create App Header
 *
 **/

const header = document.createElement('header');
header.setAttribute('id','header');
document.body.appendChild( header );

const topbar = (
	<NavBar
		id="top-bar"
		className="top-bar"
	>
		<Image
			id='header-logo'
			src='logo.svg'
			alt='Logotipo da Fundação Tide Setubal'
		/>
		<Menu
			id='header-menu'
			className='hide-for-small-only align-right'
		>
			{ navigation['header-menu'].map(
				( item, index, array ) => {
					let menuItemKey=item.caption.toLowerCase().split(/\W/).join('').trim();
					return (
						<MenuItem
							id={ menuItemKey }
							key={ menuItemKey }
						>
							{ item.link ? ( <a href={ item.link }>{ item.caption }</a> ) : item.caption }
						</MenuItem>
					);
				}
			) }
			<MenuItem
				id='summary-menu-button'
			/>
				<a
					href="#summary-menu"
					data-toggle="summary-menu-container"
				>
					<span className='dashicons dashicons-menu'></span>	
				</a>

		</Menu>
	</NavBar>
);

/**
 * Create App Canvas
 *
 **/

const board = document.createElement('main');
board.setAttribute('id','board');
document.body.appendChild( board );

const splittedManTitle = contents['main-title'].split(' ').map( ( word, index ) => {
	return (
		<span key={word.replace( /\W/, word )}>
			{word}
		</span>
		)
} );

/**
 * Display Title
 *
 **/

const display = (
	<h1
		id='main-title'
		key='main-title'
	>
		{ splittedManTitle }
	</h1>
);

/**
 * Network Graph Menu
 *
 **/

const network = (
	<NetworkGraph
		id="network"
		width="800"
		height="600"
		data={ networkData }
	/>
);


/**
 * Menu Container
 *
 **/

let modals = []

const summary = (	
	<Modal
		id='summary-menu-container'
		size='full'
	>
		<Menu
			id='summary-menu'
			className='grid-x medium-up-5'
		> 
			{ navigation['summary-menu'].map( ( menuItem, menuIndex ) => {
				let menuItemKey = 'summary-menu-' + menuItem.caption.toLowerCase().split(/\W/).join('');
				return (
					<MenuItem
						id={ menuItemKey }
						key={ menuItemKey }
						className='cell'
					>
						<a href={ menuItem.link } data-toggle={ menuItem.link }>
							{ menuItem.caption }
						</a>
						{ menuItem.children ? (
								<Menu
									id={ 'submenu-' + menuItemKey }
								>
									{ menuItem.children.map( ( subMenuItem, subMenuIndex ) => {
										let modalId = subMenuItem.link.replace(/^#/,'');
										let modalCategory = modalId.replace(/[^a-z].*/,'');
										modals.push(
											(
												<Modal
													id={ modalId }
													key={ modalId }
													size="full"
													className={"twothirds align-right modal-" + modalCategory }
												>
													<header
														className="header-modal"
													>
														<h5 className="modal-category-title">{ modalCategory }</h5>
														<h3 className="modal-title">{ subMenuItem.caption }</h3>
													</header>
													<article>
														<p>
															Teste Modal
														</p>
													</article>
												</Modal>
											)
										);
										let subMenuItemKey = 'submenu-' + menuItemKey + '-item-' + subMenuItem.caption.toLowerCase().split(/\W/).join('').trim();
										return (
											<MenuItem
												id={ subMenuItemKey }
												key={ subMenuItemKey }
											>
												<a
													data-toggle={ modalId }
													data-close=""
													href={ subMenuItem.link }>{ subMenuItem.caption }
												</a>
											</MenuItem>
										);
									} ) }
								</Menu>
							) : null }
					</MenuItem>
				);
			} ) }
		</Menu>
	</Modal>
);


/**
 * Render things
 *
 **/

const canvas = (
	<React.Fragment>
		{ display }
		{ summary }
		{ modals }
		{ network }
	</React.Fragment>
);

ReactDOM.render( topbar, document.getElementById('header') );
ReactDOM.render( canvas, document.getElementById('board') );
let bubbles = new Bubbles( 'board', { "particles": { "shape": { "image": { "src": importedImages['bubbles.svg'] } } } } );