import React from 'react';
import ReactDOM from 'react-dom';

/**
 * Menu
 *
 **/

export class Menu extends React.Component {

	constructor( props ) {
		super( props );
		this.id = this.props.id ? this.props.id : 'randomKey' + Math.random(9999);
		this.className = this.props.className || '';
		this.onClick = this.props.onClick || null;
		this.children = this.props.children;
	}

	render() {
		return (
			<ul
				id={ this.id }
				key={ this.id }
				className={ this.className.split(/menu|\s+/).concat(['menu']).join(' ').trim() }
				onClick={ this.onClick }
			>
				{ this.children }
			</ul>
		);
	}
}

export class MenuItem extends React.Component {

	constructor(props) {
		super( props );
		this.id = this.props.id || 'menuitem-' + ( this.caption || Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5) );
		this.className = this.props.className || '';
		this.children = this.props.children;
	}

	render() {
		return (
			<li
				key={ this.id }
				id={ this.id }
				className={ this.className.split(/menu-item|\s+/).concat(['menu-item']).join(' ').trim() }
			>
				{ this.children }
			</li>
		);
	}
}

export class NavBar extends React.Component {

	constructor( props ) {
		super( props );
	}

	getClassName() {
		return (this.props.className||'').split(/nav-bar|\s+/).concat(['nav-bar']).join(' ').trim();
	}

	render() {
		return (
			<nav
				id={this.props.id}
				className={ this.getClassName() }
			>
				{ this.props.children }
			</nav>
		);
	}
}

export class MenuButton extends React.Component {

	constructor( props ) {
		super( props );
		this.className = this.props.className;
	}

	render() {
		return (
			<button
				id={this.props.id}
				className={ this.className.split(/menu-button/).concat(['menu-button']).join(' ').trim() }
			>
				{ this.props.children }
			</button>
		);
	}
}

Menu.defaultProps = {
	'className': "menu"
}

MenuItem.defaultProps = {
	'className': "menu-item",
	'caption': ''
}
NavBar.defaultProps = {
	'className': "nav-bar"
}
MenuButton.defaultProps = {
	'className': "menu-button"
}