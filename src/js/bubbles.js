import particles from 'exports-loader?particlesJS=window.particlesJS,window.pJSDom!particles.js'

export class Bubbles {

	constructor( containerId, args ) {
		this.bubblesArgs = this.parseArgs();
		particles.particlesJS.load( containerId, this.bubblesArgs );
	}

	parseArgs( args, defaults ) {
		let bubblesArgs = args || {};
		let bubblesDefaults = defaults || Bubbles.defaultArgs;
		let parsedArgs = {}
		var argName;
		for( argName in bubblesDefaults ) {
			if( typeof bubblesArgs[ argName ] == 'undefined' ) {
				parsedArgs[ argName ] = bubblesDefaults[ argName ];
			}
			else if( typeof bubblesArgs[ argName ] == 'object' ){
				parsedArgs[ argName ] = this.parseArgs( bubblesArgs[ argName ], bubblesDefaults[ argName ] );
			}
			else {
				parsedArgs[ argName ] = bubblesArgs[ argName ];
			}
		}
		return "data:text/plain;base64," + btoa( JSON.stringify( parsedArgs ) );
	}
}

Bubbles.defaultArgs = {
	"particles": {
		"number": {
			"value": 100,
			"density": {
				"enable":true,
				"value_area":800
			}
		},
		"color": {
			"value": "#ffffff"
		},
		"shape": {
			"type": "circle",
			"stroke": {
				"width":0,
				"color":"#000000"
			},
			"polygon": {
				"nb_sides": 3
			},
			"image":{
				"src": "",
				"width": 3,
				"height": 7
			}
		},
		"opacity": {
			"value": 0.25,
			"random": false,
			"anim": {
				"enable": false,
				"speed":1,
				"opacity_min":0.1,
				"sync":false
			}
		},
		"size":{
			"value":15,
			"random":true,
			"anim":{
				"enable":false,
				"speed":40,
				"size_min":0.1,
				"sync":false
			}
		},
		"line_linked":{
			"enable":false,
			"distance":150,
			"color":"#ffffff",
			"opacity":0.25,
			"width":1.5
		},
		"move":{
			"enable":true,
			"speed":2,
			"direction":"right",
			"random":false,
			"straight":true,
			"out_mode":"out",
			"bounce":true,
			"attract":{
				"enable":true,
				"rotateX":600,
				"rotateY":1200
			}
		}
	},
	"interactivity":{
		"detect_on":"canvas",
		"events":{
			"onhover":{
				"enable":true,
				"mode":"repulse"
			},
			"onclick":{
				"enable":false,
				"mode":"push"
			},
			"resize":true
		},
		"modes":{
			"grab":{
				"distance":400,
				"line_linked":{
					"opacity":1
				}
			},
			"bubble":{
				"distance":200,
				"size":40,
				"duration":2,
				"opacity":3,
				"speed":1
			},
			"repulse":{
				"distance":20,
				"duration":0.1
			},
			"push":{
				"particles_nb":1
			},
			"remove":{
				"particles_nb":2
			}
		}
	},
	"retina_detect":true
};