import React from 'react';

/**
 * Modal
 *
 **/

export class Modal extends React.Component {

	constructor( props ) {
		super( props );
		this.id = this.props.id;
		this.children = this.props.children;
		this.hint = this.props.hint;
		this.closeButton = this.props['close-button'];
		this.closeOnClick = this.props['close-on-click'] || null;
		this.className = (this.props.className || '' ).split(/\s+/);
		this.size = this.props.size;
		this.multiple = this.props.multiple || 0;
		if( ['tiny', 'small', 'large', 'full'].includes( this.size ) ) {
			this.className.push( this.size );
		}
		this.animation = ( this.props.animation || '' ).split(/\s+/, 2 );
		if( this.animation.length < 2 ) {
			this.animation.push( this.animation[0] );
		}
		this.className = this.className.join(' ');
	}

	render() {
		return (
			<div
				id={ this.id }
				className={ this.className.split(/reveal|\s+/).concat( ['reveal', 'modal'] ).join(' ') }
				data-reveal=''
				data-multiple-opened={ this.multiple }
				data-close-on-click={ this.closeOnClick }
			>
				{ this.children }
				{ this.closeButton ? (
					<button
						className='close-button'
						data-close=''
						aria-label={ this.hint }
						type='button'
						style={ { zIndex: 2000 } }
					>
						<span aria-hidden={true}>
							{ typeof this.closeButton == 'string' ? this.closeButton : ( <span>&times;</span> ) }
						</span>
					</button> ) : null
				}
			</div>
		);
	}
}

Modal.defaultProps = {
	'close-button': true,
	'animation': null,
	'hint': 'click to close this dialog box'
}