import React from 'react';
import ReactDOM from 'react-dom';
import '../styles/webfonts/dashicons.css';

// sim, temos que importar um por um manualmente

import logo from '../media/logo.svg';
import comunicacao from '../media/comunicacao.svg';
import conhecimento from '../media/conhecimento.svg';
import macropolitica from '../media/macropolitica.svg';
import territorios from '../media/territorios.svg';
import bubbles from '../media/bolha-fundo.svg';

export let importedImages = {
	'logo.svg': logo,
	'comunicacao.svg': comunicacao,
	'conhecimento.svg': conhecimento,
	'macropolitica.svg': macropolitica,
	'territorios.svg': territorios,
	'bubbles.svg': bubbles
}

export class Image extends React.Component {

	constructor( props ) {
		super( props );
		this.link = this.props.link || null;
		this.className = this.props.className;
		this.container = false;
		if( this.props.container ) {
			if( typeof this.props.container == 'string' ) {
				this.container = this.props.container; 
			}
			else if( this.link ) {
				this.container = 'a';
			}
			else {
				this.container = 'figure'; 
			}
		}
		this.id = this.props.id || null;
		this.src = this.props.src;
		this.alt = this.props.alt || 'image: '+ imageSrc;
	}

	render() {

		let imageSrc = this.src;
		let ImportedImage = importedImages[ imageSrc ]; // this can be a string url or a React component class (if using react importers) -- hence pascal case;
		let imageElement = typeof ImportedImage == 'string' ? (
			<img
				id={ this.container ? null : this.id }
				className={ this.container ? null : this.className }
				src={ ImportedImage }
				alt={ this.alt }
			/>
		) : (
			<ImportedImage/>
		);
		if( this.container ) {
			let containerProps = {
				id: this.id,
				className: this.className
			}
			if( this.container == 'a' ) {
				containerProps['href'] = this.link;
			}
			imageElement = React.createElement( this.container, containerProps, imageElement );
		}
		return imageElement;
	}
}

export class Icon extends React.Component {

	constructor( props ) {
		super( props );
		this.iconset = 'dashicons';
		this.link = props.link || null;
		this.icon = props.icon;
		if( this.iconset == 'dashicons' ) {
			this.className = (this.className||'').split(/dashicons(-\w+)|\s+/).concat( [ 'icon', 'dashicons', 'dashicons-' + this.icon ] ).join(' ');
		}
		else if( this.iconset == 'foundation-icons' ) {
			this.className = (this.className||'').split(/fi-\[(-\w+)\]|\s+/).concat( [ 'fi-[' + this.icon + ']' ] ).join(' ');
		}
		this.container = this.props.container;
		if( this.container !== false || this.link ) {
			this.container = this.link ? 'a' : 'figure';
		}
		if( this.container ) {
			this.containerClassName = this.className.split(/dashicons(?:-\w+)?|icon|\s+/).concat(['icon-container']).join(' ').trim();
			this.className = 'icon dashicons dashicons-' + this.icon;
		}
		console.log( {'link': this.link, 'icon': this.icon, 'class': this.className, 'container': this.container, 'container-class': this.containerClassName } );
		this.id = this.props.id || null;
	}

	wrapInConatiner( component ) {
		if( ! this.container ) {

		}
	}

	render() {
		let imageElement = (
			<span
				id={ this.container ? null : this.id }
				className={ this.className }
			>
			</span>
		);
		if( this.container ) {
			let containerProps = {
				id: this.id,
				className: this.containerClassName
			}
			if( this.container == 'a' ) {
				containerProps['href'] = this.link;
			}
			imageElement = React.createElement( this.container, containerProps, imageElement );
		}
		return imageElement;
	}
}

Image.defaultProps = {
	'container': true
}

Icon.defaultProps = {
	'container': false,
	'icon': 'heart'
}
