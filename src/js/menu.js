import React from 'react';
import ReactDOM from 'react-dom';

/**
 * Menu
 *
 **/

export default class Menu extends React.Component {

	constructor( props ) {
		super( props );
	}

	getClassName() {
		return this.props.className.split(/(menu|\s)/).concat(['menu',this.state.showModal]).join(' ');
	}

	render() {
		return (
			<div
				id={this.props.id}
				className={ this.getClassName() }
			>
				<button
					className="close-modal"
					onClick={this.closeModal}
				>
					{this.props['data-close-button-caption']}
				</button>
				<button
					className="open-modal"
					onClick={this.openModal}
				>
					{this.props['data-open-button-caption']}
				</button>
				<div
					className='modal-content'
				>
					{ this.props.children }
				</div>
			</div>
		);
	}
}

Menu.defaultProps = {
	'className': ""
}
